from django.shortcuts import render, get_object_or_404
from .models import Service

def service_list(request):
    services = Service.objects.all()
    context = {
        'services': services,
    }
    return render(request, 'jadwal_pelayanan/service_list.html', context)

def service_details(request, service_id):
    service = get_object_or_404(Service, pk=service_id)
    context = {
        'service': service,
    }
    return render(request, 'jadwal_pelayanan/service_details.html', context)
