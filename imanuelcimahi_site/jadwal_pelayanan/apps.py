from django.apps import AppConfig


class JadwalPelayananConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'jadwal_pelayanan'
