from django.contrib import admin
from .models import Individual, Role, Service, ServiceRole
from django.contrib.admin import DateFieldListFilter
from django.utils import timezone

admin.site.register(Individual)
admin.site.register(Role)


@admin.register(Service)
class ServiceAdmin(admin.ModelAdmin):
    list_display = ['datetime', 'place', 'dresscode']

@admin.register(ServiceRole)
class ServiceRoleAdmin(admin.ModelAdmin):
    list_display = ['formatted_datetime','person_name', 'role_title', 'service_place', 'service_dresscode']
    list_filter = [
                    ('service__datetime', DateFieldListFilter),
                    'service__place'
                ]
    
    def formatted_datetime(self, obj):
        local = timezone.localtime(obj.service.datetime)
        return local.strftime('%d-%m-%Y %H:%M:%S')
    formatted_datetime.short_description = 'Datetime'
    
    def role_title(self, obj):
        return obj.role.title
    role_title.short_description = 'Role Title'
    
    def person_name(self, obj):
        return obj.person.name
    person_name.short_description = 'Person Name'
    
    def service_place(self, obj):
        return obj.service.place
    service_place.short_description = 'Service Place'

    def service_dresscode(self, obj):
        return obj.service.dresscode
    service_dresscode.short_description = 'Service Dresscode'
    
    

