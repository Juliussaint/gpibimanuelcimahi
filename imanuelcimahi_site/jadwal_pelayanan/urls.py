from django.urls import path
from . import views

app_name = 'jadwal_pelayanan'

urlpatterns = [
    path('service-list/', views.service_list, name='service-list'),
    path('service-details/<int:service_id>/', views.service_details, name='service-details'),
]
