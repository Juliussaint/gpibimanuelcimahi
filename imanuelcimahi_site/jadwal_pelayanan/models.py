from django.db import models
from django.utils import timezone

class Individual(models.Model):
    name = models.CharField(max_length=300)

    def __str__(self):
        return self.name

class Role(models.Model):
    title = models.CharField(max_length=50)
    
    def __str__(self):
        return self.title
    

class Service(models.Model):
    datetime = models.DateTimeField()
    dresscode = models.CharField(max_length=50, null=True, blank=True)
    place = models.CharField(max_length=100, null=True, blank=True)
    roles = models.ManyToManyField(Role, through='ServiceRole')

    def __str__(self):
        local_datetime = timezone.localtime(self.datetime)
        return f"{local_datetime.strftime('%d-%m-%Y %H:%M:%S')} - {self.place}"
    


class ServiceRole(models.Model):
    service = models.ForeignKey(Service, on_delete=models.CASCADE)
    role = models.ForeignKey(Role, on_delete=models.CASCADE)
    person = models.ForeignKey(Individual, on_delete=models.CASCADE)

    def __str__(self):
        local = timezone.localtime(self.service.datetime)
        return f"{local.strftime('%d-%m-%Y %H:%M:%S')} - {self.role.title} - {self.person.name} - {self.service.place} - {self.service.dresscode}"

