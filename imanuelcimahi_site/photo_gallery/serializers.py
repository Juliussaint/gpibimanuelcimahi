from rest_framework import serializers
from taggit.serializers import (TagListSerializerField, TaggitSerializer)
from photo_gallery.models import PhotoGallery



class PhotoGallerySerializer(TaggitSerializer, serializers.ModelSerializer):
    tags = TagListSerializerField()

    class Meta:
        model = PhotoGallery
        fields = ('id', 'title', 'image', 'caption', 'tags')