from django.urls import path
from . import views

app_name = 'photo_gallery'

urlpatterns = [
    path('', views.PhotoGalleryView, name= 'gallery'),
    path('photogallerylist/', views.PhotoGalleryListApiView.as_view(), name='gallery-list'),
]


