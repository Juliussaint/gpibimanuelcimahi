from django.shortcuts import render
from taggit.models import Tag
from rest_framework.generics import ListAPIView
from photo_gallery.models import PhotoGallery
from photo_gallery.serializers import PhotoGallerySerializer


def PhotoGalleryView(request):
    photos = PhotoGallery.objects.prefetch_related('tags').all()
    tags = Tag.objects.all()
    context = {'photos': photos, 'tags': tags}
    return render(request, 'photo_gallery/photogallery.html', context)


class PhotoGalleryListApiView(ListAPIView):
    queryset = PhotoGallery.objects.all()
    serializer_class = PhotoGallerySerializer
    