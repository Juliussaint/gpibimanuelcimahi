from django.db import models
from taggit.managers import TaggableManager


class PhotoGallery(models.Model):
    title = models.CharField(max_length=100)
    image = models.ImageField(upload_to='gallery')
    caption = models.CharField(max_length=200, blank=True, null=True)

    tags = TaggableManager()

    def __str__(self):
        return self.title

