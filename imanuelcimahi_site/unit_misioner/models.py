from django.db import models
from django.utils.text import slugify

class Department(models.Model):
    name = models.CharField(max_length=100)
    singkatan = models.CharField(max_length=50)
    description = models.TextField()
    logo = models.ImageField(upload_to='department_logo/', blank=True, null=True)
    banner = models.ImageField(upload_to='department_banners/', blank=True, null=True)
    slug = models.SlugField(unique=True, max_length=100)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super().save(*args, **kwargs)

    def __str__(self):
        return self.name
    
    def get_custom_order(self):
        custom_order = {
            'pklu': 1,
            'pkb': 2,
            'pkp': 3,
            'gp': 4,
            'pt': 5,
            'pa': 6,
            'peg': 7,
            'muger': 8,
            'inforkom': 9,
            'pelkes': 10,
            'teologi': 11,
            'kedukaan': 12,
            'rumahtangga': 13,
        }
        return custom_order.get(self.singkatan, 0)


class Position(models.Model):
    title = models.CharField(max_length=100)

    def __str__(self):
        return self.title

class Member(models.Model):
    name = models.CharField(max_length=100)
    department = models.ForeignKey(Department, on_delete=models.CASCADE)
    position = models.ForeignKey(Position, on_delete=models.CASCADE)
    photo = models.ImageField(upload_to='member_photos/', blank=True, null=True)
    

    def __str__(self):
        return self.name

