from django.views.generic import DetailView, ListView
from .models import Department

class DepartmentListView(ListView):
    model = Department
    template_name = 'unit_misioner/department_list.html'
    context_object_name = 'departments'

    def get_queryset(self):
        departments = super().get_queryset()
        return sorted(departments, key=lambda department: department.get_custom_order())
    

class DepartmentDetailView(DetailView):
    model = Department
    template_name = 'unit_misioner/department_detail.html'
    context_object_name = 'department'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['members'] = self.object.member_set.all()
        return context

