from django.urls import path
from .views import DepartmentListView, DepartmentDetailView

app_name = 'unit_misioner'

urlpatterns = [
    path('', DepartmentListView.as_view(), name='department-list'),
    path('departments/<slug:slug>/', DepartmentDetailView.as_view(), name='department-detail'),
]
