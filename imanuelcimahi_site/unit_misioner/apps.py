from django.apps import AppConfig


class UnitMisionerConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'unit_misioner'
