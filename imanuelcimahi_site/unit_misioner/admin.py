from django.contrib import admin
from .models import Department, Position, Member

@admin.register(Department)
class DepartmentAdmin(admin.ModelAdmin):
    list_display = ['name', 'description', 'banner', 'logo']
    list_display_links = ['name','banner', 'logo']
    prepopulated_fields = {'slug': ('name',)}

    actions = ['delete_selected_banner', 'delete_selected_logo']

    def delete_selected_media(self, request, queryset):
        for obj in queryset:
            obj.banner.delete()
            obj.logo.delete()
        self.message_user(request, "Selected media have been deleted.")

@admin.register(Position)
class PositionAdmin(admin.ModelAdmin):
    list_display = ['title']

@admin.register(Member)
class MemberAdmin(admin.ModelAdmin):
    list_display = ['name', 'department', 'position']
    list_filter = ['department', 'position']
    search_fields = ['name', 'department__name', 'position__title']

