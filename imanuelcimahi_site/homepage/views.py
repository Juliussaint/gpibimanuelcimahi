from django.shortcuts import render
from django.views import View
from events.models import Event
from unit_misioner.models import Department

from photo_gallery.models import PhotoGallery
from taggit.models import Tag
from phmj.models import AnggotaPhmj, Periode


class HomepageView(View):
    template_name = 'homepage/index.html'

    def get(self, request, *args, **kwargs):
        events = Event.objects.all()
        departments = Department.objects.all()
        sorted_departments = sorted(departments, key=lambda department: department.get_custom_order())
       
        photo = PhotoGallery.objects.all()
        tags = Tag.objects.all()
        anggotas  = AnggotaPhmj.objects.all()
        sorted_anggotas = sorted(anggotas, key=lambda anggota: anggota.get_custom_order())
        periodes = Periode.objects.first()

        context = {
            'events': events, 
            'departments': sorted_departments, 
          
            'photos' : photo,
            'tags' : tags,
            'anggotas' : sorted_anggotas,
            'periodes' : periodes,
            }
        return render(request, self.template_name, context)
    

    

   

    

