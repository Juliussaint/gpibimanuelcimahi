from django.apps import AppConfig


class PhmjConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'phmj'
