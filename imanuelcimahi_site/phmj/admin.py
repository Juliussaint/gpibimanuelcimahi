from django.contrib import admin
from .models import AnggotaPhmj, Periode

@admin.register(AnggotaPhmj)
class AnggotaAdmin(admin.ModelAdmin):
    list_display = ('nama', 'jabatan')

admin.site.register(Periode)

    

