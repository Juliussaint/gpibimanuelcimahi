from django.db import models

class Periode(models.Model):
    periode = models.CharField(max_length=50)
    def __str__(self):
        return self.periode

class AnggotaPhmj(models.Model):
    nama = models.CharField(max_length=200)
    jabatan = models.CharField(max_length=50)
    foto = models.ImageField(upload_to='phmj', blank=True, null=True)

    def __str__(self):
        return self.nama
    
    def get_custom_order(self):
        custom_order = {
            "Ketua Majelis Jemaat": 1,
            "Ketua I": 2,
            "Ketua II": 3,
            "Ketua III": 4,
            "Ketua IV": 5,
            "Ketua V": 6,
            "Sekretaris": 7,
            "Sekretaris I": 8,
            "Sekretaris II": 9,
            "Bendahara": 10,
            "Bendahara I": 11,
        }
        return custom_order.get(self.jabatan, 0)
