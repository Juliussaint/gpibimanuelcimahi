from django.shortcuts import render
from .models import AnggotaPhmj, Periode


def AnggotaPhmjView(request):
    anggotas = AnggotaPhmj.objects.all()
    sorted_anggotas = sorted(anggotas, key=lambda anggota: anggota.get_custom_order())
    
    periodes_query = Periode.objects.all()
    periodes = periodes_query.first()

    context = {'anggotas': sorted_anggotas, 'periodes': periodes}
    return render(request, 'phmj/phmj.html', context)

