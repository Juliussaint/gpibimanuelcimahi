from django.db import models

class Event(models.Model): 
    name = models.CharField(max_length=500)
    description = models.TextField()
    profile_image = models.ImageField(upload_to='events/', blank=True, null=True)
    
    
    def __str__(self):
        return self.name
