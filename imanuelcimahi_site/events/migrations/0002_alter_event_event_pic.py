# Generated by Django 4.2.4 on 2023-08-02 07:36

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='event_pic',
            field=models.ImageField(blank=True, null=True, upload_to='events/img/event/'),
        ),
    ]
